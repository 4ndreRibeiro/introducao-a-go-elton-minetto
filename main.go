package main

import (
    "fmt"
    "math"
)
func swap(x, y string)(string, string) {
    return y, x
}

func main() {
    fmt.Println("Now you have %g problems.", math.Sqrt(9))

    a, b := swap("hello", "world")
    fmt.Println(b,a)

    ok, err := say("Hello World")
    if err != nil {
        panic(err.Error())
    }
    switch ok {
        case true:
            fmt.Println("Deu certo")
        default:
            fmt.Println("deu errado")
        }
    }

    func say(what string) (bool, error) {
        if what == "" {
            return false, fmt.Errorf("Empty string")
        }
        fmt.Println(what)
        return true, nil
    }